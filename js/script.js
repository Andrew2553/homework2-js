
// 1. Числа, Строки, булевое значение, Обьекты, null, NaN, Массивы, Undefined, Function, 

// 2. Оператор == выполняет нестрогое сравнение, пытаясь привести оба значения к одному типу данных.
//    Оператор === выполняет строгое сравнение, сравнивая значения и их типы данных без приведения типов.

//3. Оператор - это символ или ключевое слово, которое выполняет операцию над одним или более операндами с целью получения результата. 

      // Типы операторов:
      // Арифметические операторы
      // Операторы сравнения
      // Логические операторы
      // Операторы присваивания
      // Условные операторы
      // Операторы инкремента и декремента








const getNameAndAge = () => {
    let nameUser = prompt('What is your name?');
    let age = prompt('What is your age?');
    
    while (!nameUser || isNaN(age)) {
      nameUser = prompt('Please enter a valid name:');
      age = prompt('Please enter a valid age:');
    }
  
    return { nameUser, age };
  };
  

  const { nameUser, age } = getNameAndAge();

  if (age < 18) {
    alert('You are not allowed to visit this website.');
  } else if (age >= 18 && age <= 22) {
    const confirmMessage = 'Are you sure you want to continue?';
    const confirmation = confirm(confirmMessage);
  
    if (confirmation) {
      alert(`Welcome, ${nameUser}!`);
    } else {
      alert('You are not allowed to visit this website.');
    }
  } else {
    alert(`Welcome, ${nameUser}!`);
  }